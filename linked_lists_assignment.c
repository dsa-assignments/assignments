#include <stdio.h>
#include <stdlib.h>

struct Node
{
        int number;                  //structure of linked list
        struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
        struct Node *head = NULL;
        int choice, data;

        while (1)
        {
                printf("Linked Lists\n");
                printf("1. Print List\n");
                printf("2. Append\n");
                printf("3. Prepend\n");
                printf("4. Delete\n");
                printf("5. Exit\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);
//Switch is used to select one of several code blocks
                switch (choice)
                {
                case 1:
                        printList(head);
                        break;
                case 2:
                        printf("Enter the value to append: ");
                        scanf("%d", &data);
                        append(&head, data);
                        break;
                case 3:
                        printf("Enter a value to prepend: ");
                        scanf("%d", &data);
                        prepend(&head, data);
                        break;
                case 4:
                        printf("Enter the value to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                case 5:
                        return 1;
//If the user enters a value other than 1, 2, 3, 4, or 5, the program prints "Invalid input."                   
                default:
                        printf("Invalid input.");
                        break;
                }
        }

        return 0;
}

struct Node *createNode(int num)
{
    // Allocate memory for a new node
    struct Node *newNode = malloc(sizeof(struct Node));
    if (newNode == NULL) {
        // Print an error message
        printf("Memory allocation failed\n");
        exit(EXIT_FAILURE); // Terminate the program
    }
    // Initialize
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}


void printList(struct Node *head)
{
    // Initialize pointer to the head of the list
    struct Node *current = head;
    printf("[ ");
    
    // Traverse the list and print each node's value
    while (current != NULL)
    {
        printf("%d, ", current->number); // Print the number stored in the current node
        current = current->next; // Move to the next node
    }
    printf(" ]\n");
}


void append(struct Node **head, int num)
{
    // Create a new node with the given num value
    struct Node *newNode = createNode(num);
    
    // If the list is empty (head is NULL), make the new node the head and return
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    
    // Traverse the list to find the last node
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    
    // Append the new node to the end of the list
    current->next = newNode;
}


void prepend(struct Node **head, int num)
{
    // Create a new node with the given num value
    struct Node *newNode = createNode(num);
    
    // Set the next pointer of the new node to point to the current head node
    newNode->next = *head;
    
    // Update the head pointer to point to the new node, making it the new head of the list
    *head = newNode;
}


void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head, *prev = NULL;
    
    // If the head node contains the key value
    if (current != NULL && current->number == key)
    {
        *head = current->next; // Update the head to point to the next node
        free(current); // Free the memory of the node to be deleted
        return; // Exit the function
    }
    
    // Traverse the list to find the node with the key value
    while (current != NULL && current->number != key)
    {
        prev = current; // Keep track of the previous node
        current = current->next; // Move to the next node
    }
    
    // If the key value was not found in the list
    if (current == NULL)
    {
        return; // Exit the function
    }
    
    // Update the 'next' pointer of the previous node to skip the node to be deleted
    prev->next = current->next;
    
    // Free the memory of the node to be deleted
    free(current);
}

void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head, *prev = NULL;
    
    // If the head node contains the value to be deleted
    if (current != NULL && current->number == value)
    {
        *head = current->next; // Update the head to point to the next node
        free(current); // Free the memory of the node to be deleted
        return; // Exit the function
    }
    
    // Traverse the list to find the node with the value to be deleted
    while (current != NULL && current->number != value)
    {
        prev = current; // Keep track of the previous node
        current = current->next; // Move to the next node
    }
    
    // If the value was not found in the list
    if (current == NULL)
    {
        return; // Exit the function
    }
    
    // Update the 'next' pointer of the previous node to skip the node to be deleted
    prev->next = current->next;
    
    // Free the memory of the node to be deleted
    free(current);
}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;
    
    // Traverse the list to find the node with the specified key
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }
    
    // If the key is not found in the list, return without inserting
    if (current == NULL)
    {
        return;
    }
    
    // Create a new node with the given value
    struct Node *newNode = createNode(value);
    
    // Insert the new node after the node with the specified key
    newNode->next = current->next; // Connect the new node to the next node
    current->next = newNode; // Connect the node with the specified key to the new node
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
    
    // Traverse the list to find the node with the specified searchValue
    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }
    
    // If the searchValue is not found in the list, return without inserting
    if (current == NULL)
    {
        return;
    }
    
    // Create a new node with the given newValue
    struct Node *newNode = createNode(newValue);
    
    // Insert the new node after the node with the specified searchValue
    newNode->next = current->next; // Connect the new node to the next node
    current->next = newNode; // Connect the node with the specified searchValue to the new node
}